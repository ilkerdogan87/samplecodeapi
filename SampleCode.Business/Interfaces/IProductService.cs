﻿using System.Collections.Generic;
using SampleCode.Cache.Interception;
using SampleCode.Global;
using SampleCode.Global.Models.RequestModels.Product;
using SampleCode.Global.Models.ViewModels.Product;

namespace SampleCode.Business.Interfaces
{
    public interface IProductService
    {
        [Caching(Duration = Consts.CacheDuration.FiveMinutes)]
        List<ProductViewModel> GetProducts(bool onlyActive = false, string key = null);

        [Caching(Duration = Consts.CacheDuration.TenMinutes)]
        ProductViewModel GetProductById(int id);
        
        bool AddOrUpdateProduct(ProductDataModel product);

        bool RemoveProduct(int id);
    }
}
