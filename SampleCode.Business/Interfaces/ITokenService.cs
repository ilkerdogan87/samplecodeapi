﻿using SampleCode.Cache.Interception;
using SampleCode.Global.Models.RequestModels.Token;

namespace SampleCode.Business.Interfaces
{
    public interface ITokenService
    {
        [Caching(Duration =Global.Consts.CacheDuration.FiveMinutes)]
        string CreateToken(TokenRequestModel request);

        //DO NOT CACHE
        bool ResolveToken(string token);
    }
}
