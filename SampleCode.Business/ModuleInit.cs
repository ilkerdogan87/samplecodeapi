﻿using SampleCode.Business.Interfaces;
using SampleCode.Business.Services;
using SampleCode.Cache.Interception;
using SampleCode.Global.Module;
using System.ComponentModel.Composition;
using Microsoft.Practices.Unity.InterceptionExtension;
using SampleCode.Log;

namespace SampleCode.Business
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(IModuleRegistrar registrar)
        {
            registrar.RegisterTypeWithInterceptor<ITokenService, TokenService>
               (
                   new Interceptor<InterfaceInterceptor>(),
                   new InterceptionBehavior<LoggingInterceptorBehavior>(),
                   new InterceptionBehavior<CachingInterceptorBehavior>()
               );

            registrar.RegisterTypeWithInterceptor<IProductService, ProductService>
                (
                    new Interceptor<InterfaceInterceptor>(),
                    new InterceptionBehavior<LoggingInterceptorBehavior>(),
                    new InterceptionBehavior<CachingInterceptorBehavior>()
                );
        }
    }
}
