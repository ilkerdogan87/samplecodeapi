﻿using System.Collections.Generic;
using System.Globalization;
using SampleCode.Global.Models.RequestModels.Product;
using SampleCode.Global.Models.ViewModels.Product;

namespace SampleCode.Business.Helpers
{
    internal static class ProductViewModelHelper
    {
        public static List<ProductViewModel> ConvertToProductViewModel(List<ProductDataModel> products)
        {
            if (products == null || products.Count == 0)
                return null;

            var data = new List<ProductViewModel>();
            foreach (var product in products)
            {
                var item = ConvertToProductViewModel(product);

                if(item != null)
                    data.Add(item);
            }

            return data;
        }

        public static ProductViewModel ConvertToProductViewModel(ProductDataModel product)
        {
            if (product == null)
                return null;

            return new ProductViewModel
            {
                Price = product.Price.ToString(CultureInfo.InvariantCulture),
                Name = product.Name,
                Photo = product.Photo,
                Id = product.Id,
                IsActive = product.IsActive
            };
        }
    }
}
