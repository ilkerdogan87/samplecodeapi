﻿using SampleCode.Business.Helpers;
using SampleCode.Business.Interfaces;
using SampleCode.Data.Interfaces;
using System;
using System.Collections.Generic;
using SampleCode.Global.Models.RequestModels.Product;
using SampleCode.Global.Models.ViewModels.Product;

namespace SampleCode.Business.Services
{
    public class ProductService : IProductService
    {
        private readonly Lazy<IProductData> _productData;

        public ProductService(Lazy<IProductData> productData)
        {
            _productData = productData;
        }

        public List<ProductViewModel> GetProducts(bool onlyActive, string key)
        {
            var products = _productData.Value.GetProducts(onlyActive,key);
            return ProductViewModelHelper.ConvertToProductViewModel(products);
        }

        public ProductViewModel GetProductById(int id)
        {
            var product = _productData.Value.GetProductById(id);

            return ProductViewModelHelper.ConvertToProductViewModel(product);
        }
        

        public bool AddOrUpdateProduct(ProductDataModel product)
        {
            return _productData.Value.AddOrUpdateProduct(product);
        }

        public bool RemoveProduct(int id)
        {
            return _productData.Value.RemoveProduct(id);
        }
    }
}
