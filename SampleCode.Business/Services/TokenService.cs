﻿using SampleCode.Business.Interfaces;
using SampleCode.Global.Helpers;
using System;
using System.Text;
using SampleCode.Data.Interfaces;
using SampleCode.Global.Models;
using SampleCode.Global.Models.RequestModels.Token;

namespace SampleCode.Business.Services
{
    public class TokenService : ITokenService
    {
        private readonly Lazy<ITokenData> _tokenData;

        public TokenService(Lazy<ITokenData> tokenData)
        {
            _tokenData = tokenData;

        }

        public string CreateToken(TokenRequestModel request)
        {
            GeneralFunctions.AddItemsToCurrentRequest(request);

            if (string.IsNullOrWhiteSpace(request.UserName) || string.IsNullOrWhiteSpace(request.Password)
                || string.IsNullOrWhiteSpace(request.Platform))
                throw new ApiException(Global.Enums.MessageType.TokenCannotCreate);

            var encryptedPass = CryptoHelper.EncryptText(request.Password, ConfigHelper.WebApiCryptoPass);
            var isUserExist = _tokenData.Value.CheckIfUserExist(request.UserName, encryptedPass);
            
            if (!isUserExist)
                throw new ApiException(Global.Enums.MessageType.TokenCannotCreate);
            

            var sb = new StringBuilder();
            sb.Append(DateTime.Now.Ticks).Append(",")
              .Append(request.UserName).Append(",")
              .Append(encryptedPass).Append(",")
              .Append(DateTime.Now.AddHours(1)).Append(",")
              .Append(request.Platform).Append(",")
              .Append(request.ClientIp).Append(",");

            var input = sb.ToString();

            return CryptoHelper.EncryptText(input, ConfigHelper.WebApiCryptoPass);
        }

        public bool ResolveToken(string token)
        {
            var tokenModel = GetTokenModel(token);
            var response = false;

            if (tokenModel == null)
                return response;
            
            var isUserExist = _tokenData.Value.CheckIfUserExist(tokenModel.UserName, tokenModel.Password);

            if (!isUserExist || DateTime.Parse(tokenModel.Time) < DateTime.Now
                || string.IsNullOrWhiteSpace(tokenModel.Platform))
                response = false;
            else
            {
                GeneralFunctions.AddItemsToCurrentRequest(tokenModel);
                response = true;
            }
            return response;
        }

        private TokenRequestModel GetTokenModel(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                return null;

            var result = CryptoHelper.DecryptText(token,ConfigHelper.WebApiCryptoPass);

            if (string.IsNullOrWhiteSpace(result))
                return null;

            var splitted = result.Split(',');
            

            return new TokenRequestModel
            {
                UserName = splitted[1],
                Password = splitted[2],
                Time = splitted[3],
                Platform = splitted[4],
                ClientIp = splitted[5]
            };
        }

    }
}
