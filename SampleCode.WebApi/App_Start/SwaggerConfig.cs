using System.Web.Http;
using WebActivatorEx;
using SampleCode.WebApi;
using Swashbuckle.Application;
using SampleCode.WebApi.Helpers;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace SampleCode.WebApi
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                  .EnableSwagger(c =>
                  {
                      c.SingleApiVersion("v1", "SampleCode.WebApi");
                      c.IncludeXmlComments(string.Format(@"{0}\bin\SampleCode.WebApi.XML",
                                           System.AppDomain.CurrentDomain.BaseDirectory));
                      c.OperationFilter<AddRequiredHeaderParameter>();
                  })
                  .EnableSwaggerUi();
        }
    }
}
