﻿using SampleCode.WebApi.Helpers;
using System.Web.Http;

namespace SampleCode.WebApi.App_Start
{
    public class FilterConfig
    {
        internal static void RegisterGlobalFilters(HttpConfiguration configuration)
        {
            configuration.Filters.Add(new TokenAttribute());
            configuration.Filters.Add(new BasicErrorAttribute());
        }
    }
}