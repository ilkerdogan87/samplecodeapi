﻿using SampleCode.Business.Interfaces;
using SampleCode.Global;
using SampleCode.Global.Helpers;
using SampleCode.Global.Models.ViewModels;
using SampleCode.Global.Module;
using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using SampleCode.Global.Models;
using SampleCode.Global.Models.RequestModels.Token;

namespace SampleCode.WebApi.Controllers
{
    /// <summary>
    /// Token Service
    /// </summary>
    [RoutePrefix("api")]
    public class TokenController : ApiController
    {
        private readonly Lazy<ITokenService> _tokenService;
        public TokenController()
        {
            _tokenService = ModuleResolver.LazyResolve<ITokenService>();
        }

        /// <summary>
        /// To call other services, you need to create a token
        /// </summary>
        /// <param name="request">
        /// userName: use the userName that provided by system admin 
        /// password : use the password that provided by system admin
        /// </param>
        /// <returns>
        /// a hashed key that you must use for another services calls headers as the name of token
        /// </returns>
        [HttpPost]
        [Route("token")]
        public IHttpActionResult Token([FromBody]TokenApiRequestModel request)
        {
            var availableHeader = false;

            var model = CheckHeaderValues(ref availableHeader);
            if (!availableHeader)
                throw new ApiException(Enums.MessageType.TokenCannotCreate, true);

            model.UserName = request.UserName;
            model.Password = request.Password;
            
            var token = _tokenService.Value.CreateToken(model);
            
            return Ok(new BaseResponseModel<string> { Code = Consts.Code.Success, Result = token });
        }

        private TokenRequestModel CheckHeaderValues(ref bool isAvailable)
        {
            isAvailable = false;

            var platform = HttpContext.Current.Request.Headers.GetValues(Consts.SampleCodeHeaderKeys.Platform);
            if (platform == null || platform.FirstOrDefault() == null || string.IsNullOrWhiteSpace(platform.FirstOrDefault()) || platform.Equals("/"))
                return null;

            isAvailable = true;

            return  new TokenRequestModel
            {
                Platform = platform.FirstOrDefault(),
                ClientIp = GeneralFunctions.ClientIp(),
            };
        }
    }
}
