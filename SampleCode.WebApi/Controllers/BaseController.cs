﻿using SampleCode.Global.Module;
using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace SampleCode.WebApi.Controllers
{
    /// <summary>
    /// Controller lar için kullanılan ortak methodlar burada toplanır
    /// </summary>
    public class BaseController : ApiController
    {
        
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        internal string Token
        {
            get
            {
                try
                {
                    return HttpContext.Current.Request.Headers.GetValues("token").FirstOrDefault();
                }
                catch (Exception)
                {

                    return string.Empty;
                }
            }
        }

        internal Lazy<T> GetService<T>()
        {
            return ModuleResolver.LazyResolve<T>();
        }
    }
}
