﻿using SampleCode.Business.Interfaces;
using SampleCode.Global;
using SampleCode.Global.Models.ViewModels;
using SampleCode.Global.Module;
using System;
using System.Collections.Generic;
using System.Web.Http;
using SampleCode.Global.Models.RequestModels.Product;
using SampleCode.Global.Models.ViewModels.Product;

namespace SampleCode.WebApi.Controllers
{
    /// <summary>
    /// Product service
    /// </summary>
    [RoutePrefix("api/product")]
    public class ProductController : BaseController
    {
        private readonly Lazy<IProductService> _productService;
        public ProductController()
        {
            _productService = ModuleResolver.LazyResolve<IProductService>();
        }

        
        /// <summary>
        /// To get all products
        /// </summary>
        /// <returns>
        /// returns all product
        /// </returns>
        [HttpGet]
        [Route("getProducts")]
        public IHttpActionResult GetProducts(bool onlyActive = false, string key=null)
        {
            var response = _productService.Value.GetProducts(onlyActive,key);

            return Ok(new BaseResponseModel<List<ProductViewModel>> { Code = Consts.Code.Success, Result = response });
        }

        /// <summary>
        /// GetProductById
        /// </summary>
        /// <param name="id">productId</param>
        /// <returns>product detail</returns>
        [HttpGet]
        [Route("getById")]
        public IHttpActionResult GetProductById(int id)
        {
            var response = _productService.Value.GetProductById(id);

            return Ok(new BaseResponseModel<ProductViewModel> { Code = Consts.Code.Success, Result = response });
        }

        /// <summary>
        /// Add if product not exists
        /// Update if product exists
        /// </summary>
        /// <param name="product">product detail</param>
        /// <returns>true if success</returns>
        [HttpPost]
        [Route("addOrUpdate")]
        public IHttpActionResult AddOrUpdate([FromBody]ProductDataModel product)
        {
            var response = _productService.Value.AddOrUpdateProduct(product);

            return Ok(new BaseResponseModel<bool> { Code = Consts.Code.Success, Result = response });
        }

        /// <summary>
        /// removeById
        /// </summary>
        /// <param name="id">product Id</param>
        /// <returns>true if success</returns>
        [HttpPost]
        [Route("removeById")]
        public IHttpActionResult RemoveById([FromBody]int id)
        {
            var response = _productService.Value.RemoveProduct(id);

            return Ok(new BaseResponseModel<bool> { Code = Consts.Code.Success, Result = response });
        }
    }
}
