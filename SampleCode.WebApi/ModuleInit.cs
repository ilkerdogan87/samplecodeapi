﻿using SampleCode.Global.Module;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.Web;


namespace SampleCode.WebApi
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(IModuleRegistrar registrar)
        {
            registrar.RegisterType<HttpContextBase>(new InjectionFactory((c, t, n) => new HttpContextWrapper(HttpContext.Current)));
        }
    }
}