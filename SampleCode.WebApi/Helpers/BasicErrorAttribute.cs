﻿using SampleCode.Global.Models.ViewModels;
using System.Net;
using System.Web.Http.Filters;
using SampleCode.Global;
using System.Net.Http;
using SampleCode.Global.Models;
using SampleCode.Global.Module;
using SampleCode.Log;

namespace SampleCode.WebApi.Helpers
{
    internal class BasicErrorAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// Tüm WebApi katmanı hataları burada toplanır. CreateActionError metodu yeni bir action eklendiğinde güncellemeli.
        /// Aksi durumda hata kodu numarası null olarak döner
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var actionName = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;
            var controllerName = actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName;

            var errorNumber = CreateActionError(controllerName, actionName);

            var errorMessage = string.Empty;

            if (actionExecutedContext.Exception is ApiException)
            {
                var ex = (actionExecutedContext.Exception as ApiException);
                errorMessage = ex.Message;
                if (ex.IsUILayer)
                {
                    var logger = ModuleResolver.LazyResolve<ILogger>();
                    logger.Value.Error(new LogModel { Message = errorMessage, MethodName = actionName });
                }
            }
            else
            {
                errorMessage = actionExecutedContext.Exception.Message;
            }

            actionExecutedContext.ActionContext.Response = actionExecutedContext.ActionContext.Request.CreateResponse(
                       HttpStatusCode.OK,
                       new BaseResponseModel<string> { Code = Consts.Code.Error, ErrorNumber = errorNumber, Message = errorMessage },
                       actionExecutedContext.ActionContext.ControllerContext.Configuration.Formatters.JsonFormatter);

            base.OnException(actionExecutedContext);
        }

        private string CreateActionError(string controllerName, string actionName)
        {
            var errorNumber = string.Empty;

            switch (controllerName)
            {
                case "Product":
                    switch (actionName)
                    {
                        case "GetProducts":
                            errorNumber = Consts.ErrorNumbers.GetProducts;
                            break;
                    }
                    break;

                case "Token":
                    switch (actionName)
                    {
                        case "Token":
                            errorNumber = Consts.ErrorNumbers.Token;
                            break;
                    }
                    break;
            }

            return errorNumber;
        }
    }
}