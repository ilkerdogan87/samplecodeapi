﻿using EricssonTpm.Global;
using EricssonTpm.Global.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Util;

namespace EricssonTpm.WebApi.Helpers
{
    public class CustomRequestValidation : RequestValidator
    {
        protected override bool IsValidRequestString(
            HttpContext context,
            string value,
            RequestValidationSource requestValidationSource,
            string collectionKey,
            out int validationFailureIndex)
        {
            validationFailureIndex = -1;

            if (value.Contains("<%"))
            {
                context.Response.Write(new BaseResponseModel<string> { Code = Consts.Code.Error});
                return false;
            }
            else // Leave any further checks to ASP.NET. 
            {
                return base.IsValidRequestString(
                    context,
                    value,
                    requestValidationSource,
                    collectionKey,
                    out validationFailureIndex);
            }
        }
    }
}