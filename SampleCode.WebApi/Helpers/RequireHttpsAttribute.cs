﻿using EricssonTpm.Business.Helpers;
using EricssonTpm.Business.Interfaces;
using EricssonTpm.Business.Services;
using EricssonTpm.Global;
using EricssonTpm.Global.Models;
using EricssonTpm.Global.Models.Response;
using EricssonTpm.Global.Module;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace EricssonTpm.WebApi.Helpers
{
    public class RequireHttpsAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                //actionContext.Response = actionContext.Request.CreateResponse(
                //       HttpStatusCode.Forbidden,
                //       new BaseResponseModel<string> { Code = Consts.Code.Error, Message = Consts.Messages[Enums.MessageType.AllowOnlyHttps] },
                //       actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
            }
        }
    }
}