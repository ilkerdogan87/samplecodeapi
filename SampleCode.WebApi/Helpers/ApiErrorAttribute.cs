﻿using System.Web.Mvc;

namespace SampleCode.WebApi.Helpers
{
    public class ApiErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext != null && filterContext.HttpContext != null && filterContext.Exception != null)
            {

            }

            base.OnException(filterContext);
        }
    }
}