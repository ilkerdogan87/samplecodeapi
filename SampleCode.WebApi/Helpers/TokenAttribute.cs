﻿using SampleCode.Business.Interfaces;
using SampleCode.Global;
using SampleCode.Global.Models.ViewModels;
using SampleCode.Global.Module;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace SampleCode.WebApi.Helpers
{
    public class TokenAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var actionName = actionContext.ActionDescriptor.ActionName;
            var controllerName = actionContext.ControllerContext.ControllerDescriptor.ControllerName;

            if (controllerName != "Token" && actionName != "Token"  )
            {
                try
                {
                    var token = actionContext.Request.Headers.GetValues("token").FirstOrDefault();
                    //var deviceId = actionContext.Request.Headers.GetValues("deviceId").FirstOrDefault();
                    //var platformType = actionContext.Request.Headers.GetValues("platform").FirstOrDefault();

                    var service = ModuleResolver.LazyResolve<ITokenService>();

                    bool isSecuredApplication = service.Value.ResolveToken(token);

                    if (!isSecuredApplication)
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(
                        HttpStatusCode.OK,
                        new BaseResponseModel<string> { Code = Consts.Code.InvalidToken, Message = Consts.Messages[Enums.MessageType.TokenErrorMessage] },
                        actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                    }
                }
                catch (Exception ex)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(
                    HttpStatusCode.OK,
                    new BaseResponseModel<string> { Code = Consts.Code.InvalidToken, Message = Consts.Messages[Enums.MessageType.TokenErrorMessage] },
                    actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                }
            }
            
        }
    }
}