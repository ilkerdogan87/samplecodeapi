﻿using SampleCode.Global;
using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.Web.Http.Description;

namespace SampleCode.WebApi.Helpers
{
    /// <summary>
    /// Swagger Header Parametreleri
    /// </summary>
    public class AddRequiredHeaderParameter : IOperationFilter
    {
        /// <summary>
        /// Swagger Header Parametreleri Required
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="schemaRegistry"></param>
        /// <param name="apiDescription"></param>
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
                operation.parameters = new List<Parameter>();

            if (operation.operationId == "Token_Token")  // controller and action name
            {
                operation.parameters.Add(new Parameter
                {
                    name = Consts.SampleCodeHeaderKeys.Platform,
                    @in = "header",
                    type = "string",
                    description = Consts.SampleCodeHeaderKeys.Platform,
                    required = true
                });
            }

            if (operation.operationId != "Token_Token")  // controller and action name
            {
                operation.parameters.Add(new Parameter
                {
                    name = "token",
                    @in = "header",
                    type = "string",
                    description = "Token",
                    required = true
                });
            }

            if (operation.operationId == "fileupload bulunan metodlar için")  // controller and action name
            {
                operation.consumes.Add("multipart/form-data");

                operation.parameters.Add(new Parameter
                {
                    name = "file",
                    @in = "formData",
                    required = true,
                    type = "file"
                });
            }
        }
    }
}