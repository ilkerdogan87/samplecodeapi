﻿namespace SampleCode.Global.Module
{
    public interface IModule
    {
        void Initialize(IModuleRegistrar registrar);
    }
}
