﻿namespace SampleCode.Global
{
    public class Enums
    {
        public enum MessageType
        {
            TokenCannotCreate,
            TokenErrorMessage,
            DefaultErrorMessage
        }
        
        public enum PlatformType
        {
            Ios,
            Android,
            Web
        }

        public enum LogType
        {
            Info,
            Warn,
            Error
        }
    }
}
