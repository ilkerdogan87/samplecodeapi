﻿using System.Collections.Generic;

namespace SampleCode.Global
{
    public static class Consts
    {
        public static Dictionary<Enums.MessageType, string> Messages = new Dictionary<Enums.MessageType, string>() {
                {Enums.MessageType.TokenCannotCreate,"Token cannot create! Please check your userName and password!"},
                {Enums.MessageType.TokenErrorMessage,"Invalid token!"},
                {Enums.MessageType.DefaultErrorMessage,"Unexpected error occured!"}

            };


        public static class Code
        {
            public const string InvalidToken = "INVALIDTOKEN";
            public const string Success = "SUCCESS";
            public const string Error = "ERROR";
        }

        public static class CacheDuration
        {
            public const int OneMinute = 60;
            public const int FiveMinutes = 300;
            public const int TenMinutes = 600;
            public const int HalfAnHour = 1800;
            public const int OneHour = 3600;
            public const int TwoHour = 7200;
            public const int SixHours = 21600;
            public const int HalfDay = 43200;
            public const int OneDay = 86400;
        }

        public class ErrorNumbers
        {
            public const string Token = "t-1";
            public const string GetProducts = "t-2";
        }

        public class SampleCodeHeaderKeys
        {
            public const string Platform = "platform";
        }

    }
}
