﻿using Newtonsoft.Json;

namespace SampleCode.Global.Models.ViewModels.Token
{
    public class TokenResponseModel
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}
