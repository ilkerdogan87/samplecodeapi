﻿namespace SampleCode.Global.Models.ViewModels
{
    public class FileResponseModel
    {
        public byte[] File { get; set; }
        public string Extention { get; set; }
        public string BlobType { get; set; }
    }
}
