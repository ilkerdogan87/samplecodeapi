﻿using Newtonsoft.Json;

namespace SampleCode.Global.Models.ViewModels
{
    public class StatusResponseModel
    {
        [JsonProperty(PropertyName = "status")]
        public bool Status { get; set; }
    }
}
