﻿using Newtonsoft.Json;

namespace SampleCode.Global.Models.ViewModels
{
    public class BaseResponseModel<T>
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "errorMessage")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "errorNumber")]
        public string ErrorNumber { get; set; }

        [JsonProperty(PropertyName = "result")]
        public T Result { get; set; }
    }
}
