﻿namespace EricssonTpm.Global.Models
{
    public class LogModel
    {
        public Enums.LogType EventType { get; set; }
        public Enums.PlatformType Platform { get; set; }
        public string MethodName { get; set; }
        public string DeviceId { get; set; }
        public string IpAddress { get; set; }
        public object Exception { get; set; }
        public object Extra { get; set; }
    }
}
