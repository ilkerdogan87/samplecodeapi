﻿using System;

namespace SampleCode.Global.Models
{
    public class ApiException : Exception
    {
        public Enums.MessageType MessageType { get; set; }
        public string Message { get; set; }
        public bool IsUILayer { get; set; }

        public ApiException(Enums.MessageType messageType, bool isUiLayer = false)
        {
            MessageType = messageType;
            Message = Consts.Messages[MessageType];
            IsUILayer = isUiLayer;
        }
    }
}
