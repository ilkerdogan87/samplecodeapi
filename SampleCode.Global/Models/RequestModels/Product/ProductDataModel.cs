﻿namespace SampleCode.Global.Models.RequestModels.Product
{
    public class ProductDataModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Photo { get; set; }
        public bool IsActive { get; set; }
    }
}
