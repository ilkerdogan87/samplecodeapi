﻿namespace SampleCode.Global.Models.RequestModels.Token
{
    public class TokenRequestModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Platform { get; set; }
        public string ClientIp { get; set; }
        public string Time { get; set; }
    }
}
