﻿using Newtonsoft.Json;

namespace SampleCode.Global.Models.RequestModels.Token
{
    public class TokenApiRequestModel
    {
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}
