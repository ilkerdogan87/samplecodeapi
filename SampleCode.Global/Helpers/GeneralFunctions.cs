﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using SampleCode.Global.Models.RequestModels.Token;
using static SampleCode.Global.Enums;

namespace SampleCode.Global.Helpers
{

    public static class GeneralFunctions
    {
        public static string ToJSON(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static string ClientIp()
        {
            return HttpContext.Current.Request.UserHostAddress;
        }

        public static bool EqualsIgnoreCase(string first, string second)
        {
            return string.Equals(first, second, StringComparison.OrdinalIgnoreCase);
        }
        

        public static PlatformType ConvertToPlatformType(string platformType)
        {
            switch (platformType.ToUpper())
            {
                case "IOS":
                    return PlatformType.Ios;

                case "ANDROID":
                    return PlatformType.Android;

                case "WEB":
                    return PlatformType.Web;
            }

            return PlatformType.Web;
        }
        
        public static void AddItemsToCurrentRequest(TokenRequestModel model)
        {
            HttpContext.Current.Items.Remove("tokenModel");
            HttpContext.Current.Items.Add("tokenModel",model);
        }
        
        public static List<T>  NullCheckForList<T>(List<T> list) 
        {
            if (list == null || list.Count < 1)
                return null;

            return list;
        }
    }
}
