﻿using SampleCode.Global.Extentions;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using System.Reflection;

namespace SampleCode.Global.Helpers
{
    public static class ReflectionHelper
    {
        private static readonly ConcurrentDictionary<string, Type> TypesCached = new ConcurrentDictionary<string, Type>();
        private static Type GetType(string fullyQualifiedName)
        {
            if (string.IsNullOrWhiteSpace(fullyQualifiedName))
                throw new ArgumentNullException(nameof(fullyQualifiedName));

            Type t;
            if (TypesCached.TryGetValue(fullyQualifiedName, out t))
                return t;

            t = Type.GetType(fullyQualifiedName);

            if (t != null)
            {
                TypesCached.TryAdd(fullyQualifiedName, t);
                return t;
            }

            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                t = asm.GetType(fullyQualifiedName);

                if (t == null)
                    continue;

                TypesCached.TryAdd(fullyQualifiedName, t);
                break;
            }

            return t;
        }

        private static object GetValue(Expression p)
        {
            object val;
            var nodeType = p.NodeType;

            switch (nodeType)
            {
                case ExpressionType.Constant:
                    {
                        val = ((ConstantExpression)p).Value;
                    }
                    break;
                case ExpressionType.MemberInit:
                    {
                        var expression = (MemberInitExpression)p;

                        var type = expression.Type;
                        var instance = Activator.CreateInstance(type);

                        foreach (var binding in expression.Bindings)
                        {
                            var bindingValue = GetValue(((MemberAssignment)binding).Expression);
                            var prop = type.GetProperty(binding.Member.Name);

                            if (prop == null)
                                continue;

                            var propType = prop.PropertyType;
                            var realType = Nullable.GetUnderlyingType(propType) ?? propType;
                            var propValue = Convert.ChangeType(bindingValue, realType);
                            prop.SetValue(instance, propValue, null);
                        }

                        val = instance;
                    }
                    break;
                case ExpressionType.Convert:
                    {
                        var operand = ((UnaryExpression)p).Operand;
                        val = GetValue(operand);
                    }
                    break;
                case ExpressionType.ListInit:
                    {
                        var expression = (ListInitExpression)p;

                        var type = expression.Type;
                        var instance = Activator.CreateInstance(type);

                        foreach (var initializer in expression.Initializers)
                        {
                            foreach (var argument in initializer.Arguments)
                            {
                                initializer.AddMethod.Invoke(instance, new[] { GetValue(argument) });
                            }
                        }

                        val = instance;
                    }
                    break;
                default:
                    val = Expression.Lambda<Func<object>>(Expression.Convert(p, typeof(object))).Compile().Invoke();
                    break;
            }

            return val;
        }

        /// <summary>
        /// Generates a key which can be used as a Cache Key which can also be used to invoke the method again.
        /// Needs MS Unity
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GenerateKey(IMethodInvocation input)
        {
            string fullMethodName;

            if (input.MethodBase.DeclaringType != null)
                fullMethodName = input.MethodBase.DeclaringType.FullName + "$" + input.MethodBase.Name;
            else
                fullMethodName = input.MethodBase.Name;

            var parameters = input.MethodBase.GetParameters();

            var info = "";

            for (int i = 0; i < parameters.Length; i++)
            {
                var p = parameters[i];
                var pType = p.ParameterType;
                var argument = input.Arguments[i];

                if (pType.IsSimpleType())
                {
                    var o = argument ?? " ";
                    info += pType.FullName + "##" + o + "###";
                }
                else
                {
                    var ser = JsonConvert.SerializeObject(argument);
                    info += pType.FullName + "##" + ser + "###";
                }
            }

            var result = fullMethodName;

            if (string.IsNullOrWhiteSpace(info) == false)
                result += "$" + info.TrimEnd('#');

            return result;
        }

        /// <summary>
        /// GetInputParametersWithValues
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetInputParametersWithValues(IMethodInvocation input)
        {
            var parameters = input.MethodBase.GetParameters();

            var info = "";

            for (int i = 0; i < parameters.Length; i++)
            {
                var p = parameters[i];
                var pType = p.ParameterType;
                var argument = input.Arguments[i];

                if (pType.IsSimpleType())
                {
                    var o = argument ?? " ";
                    info += p.Name + ":"+o+",";
                }
                else
                {
                    var ser = JsonConvert.SerializeObject(argument);
                    info += pType.FullName + ":" + ser + ",";
                }
            }

            if (info == null)
                return null;

            return info.TrimEnd(',');
        }

        public static string GenerateKey<T>(Expression<Action<T>> expression)
        {
            var call = expression.Body as MethodCallExpression;
            if (call == null || call.Method.ReflectedType == null)
                return null;

            var fullMethodName = call.Method.ReflectedType.FullName + "$" + call.Method.Name;
            var parameters = call.Arguments;

            var info = "";

            foreach (var p in parameters)
            {
                var pType = p.Type;

                if (pType.IsSimpleType())
                {
                    var val = GetValue(p);
                    var o = val ?? " ";
                    info += pType.FullName + "##" + o + "###";
                }
                else
                {
                    var val = GetValue(p);
                    var ser = JsonConvert.SerializeObject(val);
                    info += pType.FullName + "##" + ser + "###";
                }
            }

            var result = fullMethodName;

            if (string.IsNullOrWhiteSpace(info) == false)
                result += "$" + info.TrimEnd('#');

            return result;
        }

        public static void InvokeByKey(IUnityContainer container, string key)
        {
            var parts = key.Split('$');

            var refInterface = parts[0];
            var refMethodName = parts[1];
            var hasParameters = parts.Length >= 3;

            var instance = GetType(refInterface);
            var implementation = container.Resolve(instance);

            object[] parameters = null;
            Type[] types = null;

            if (hasParameters)
            {
                var refValue = parts[2];
                var refParameters = refValue.Split(new[] { "###" }, StringSplitOptions.None);

                parameters = new object[refParameters.Length];
                types = new Type[refParameters.Length];

                for (var i = 0; i < refParameters.Length; i++)
                {
                    var parameter = refParameters[i];

                    var pParts = parameter.Split(new[] { "##" }, StringSplitOptions.None);
                    var pType = pParts[0];
                    var pValue = pParts[1];

                    var type = GetType(pType);
                    types[i] = type;

                    if (type.IsSimpleType())
                    {
                        object param;

                        if (string.IsNullOrWhiteSpace(pValue))
                        {
                            param = Activator.CreateInstance(type);
                        }
                        else
                        {
                            type = Nullable.GetUnderlyingType(type) ?? type;
                            param = type.IsEnum ? Enum.Parse(type, pValue) : Convert.ChangeType(pValue, type);
                        }

                        parameters[i] = param;
                    }
                    else
                    {
                        var param = JsonConvert.DeserializeObject(pValue, type);
                        parameters[i] = param;
                    }
                }
            }

            MethodInfo method;

            if (types != null)
                method = implementation.GetType().GetMethod(refMethodName, types);
            else
                method = implementation.GetType().GetMethod(refMethodName);

            var result = method.Invoke(implementation, parameters);
        }

    }
}
