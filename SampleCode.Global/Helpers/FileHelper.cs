﻿using System.IO;

namespace SampleCode.Global.Helpers
{
    public static class FileHelper
    {
        public static bool CheckTigrisImageFile(string mimeType)
        {
            var mimeTypeLowerCase = mimeType.ToLower();
            return !(!mimeTypeLowerCase.Contains("image") && !mimeTypeLowerCase.Contains("gif"));
        }

        public static byte[] StreamToByteArray(Stream stream)
        {
            try
            {
                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    return ms.ToArray();
                }
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }
    }
}
