﻿using System;
using System.Configuration;

namespace SampleCode.Global.Helpers
{
    public class ConfigHelper
    {
        private static string GetKey(string key)
        {
            var setting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(setting))
                return "";
            return setting;
        }

        private static string _webApiCryptoPass;
        public static string WebApiCryptoPass
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_webApiCryptoPass) == false)
                    return _webApiCryptoPass;

                _webApiCryptoPass = GetKey("WebApiCryptoPass");
                return _webApiCryptoPass;
            }
        }

        private static bool? _logServiceRequests;
        public static bool LogServiceRequests
        {
            get
            {
                if (_logServiceRequests.HasValue)
                    return _logServiceRequests.Value;

                var key = GetKey("LogServiceRequests");

                if (!string.IsNullOrWhiteSpace(key))
                {
                    try
                    {
                        _logServiceRequests = bool.Parse(key);
                    }
                    catch (Exception)
                    {
                        _logServiceRequests = false;
                    }
                }
                else
                {
                    _logServiceRequests = false;
                }
                return _logServiceRequests.Value;
            }
        }

        private static bool? _useCache;
        public static bool UseCache
        {
            get
            {
                if (_useCache.HasValue)
                    return _useCache.Value;

                var key = GetKey("UseCache");

                if (!string.IsNullOrWhiteSpace(key))
                {
                    try
                    {
                        _useCache = bool.Parse(key);
                    }
                    catch (Exception)
                    {
                        _useCache = false;
                    }
                }
                else
                {
                    _useCache = false;
                }
                return _useCache.Value;
            }
        }
        

    }
}
