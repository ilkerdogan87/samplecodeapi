﻿using SampleCode.Global.Module;
using SampleCode.Data.Interfaces;
using Microsoft.Practices.Unity.InterceptionExtension;
using System.ComponentModel.Composition;
using SampleCode.Data.DbData;

namespace SampleCode.Data
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(IModuleRegistrar registrar)
        {
            registrar.RegisterTypeWithInterceptor<ITokenData, TokenData>(
                new Interceptor<InterfaceInterceptor>()
            );

            registrar.RegisterTypeWithInterceptor<IProductData, ProductData>(
                new Interceptor<InterfaceInterceptor>()
            );
            
        }
    }
}
