﻿namespace SampleCode.Data.Interfaces
{
    public interface ITokenData
    {
        bool CheckIfUserExist(string userName,string password);
    }
}
