﻿using System.Collections.Generic;
using SampleCode.Global.Models.RequestModels.Product;

namespace SampleCode.Data.Interfaces
{
    public interface IProductData
    {
        List<ProductDataModel> GetProducts(bool onlyActive, string key);
        
        ProductDataModel GetProductById(int id);

        bool AddOrUpdateProduct(ProductDataModel product);
        bool RemoveProduct(int id);
    }
}
