﻿using System;
using System.Collections.Generic;
using SampleCode.Data.EF;
using SampleCode.Global.Models.RequestModels.Product;

namespace SampleCode.Data.Helpers
{
    public static partial class Mapper
    {
        public static List<ProductDataModel> MapTo(List<Product> entities)
        {
            if (entities == null || entities.Count == 0)
                return null;

            var data = new List<ProductDataModel>();
            foreach (var entity in entities)
            {
                var item = MapTo(entity);

                if(item != null)
                    data.Add(item);
            }

            return data;
        }

        public static ProductDataModel MapTo(Product entity)
        {
            if (entity == null)
                return null;

            return new ProductDataModel
            {
                Id = entity.Id,
                Price = entity.Price,
                Name = entity.Name,
                Photo = entity.Photo,
                IsActive = entity.IsActive
            };

        }

        public static List<Product> MapTo(List<ProductDataModel> entities)
        {
            if (entities == null || entities.Count == 0)
                return null;

            var data = new List<Product>();
            foreach (var entity in entities)
            {
                var item = MapTo(entity);

                if (item != null)
                    data.Add(item);
            }

            return data;
        }

        public static Product MapTo(ProductDataModel entity)
        {
            if (entity == null)
                return null;

            return new Product
            {
                Id = entity.Id,
                Price = entity.Price,
                Name = entity.Name,
                Photo = entity.Photo,
                LastUpdated = DateTime.UtcNow,
                IsActive = entity.IsActive
            };

        }
    }
}
