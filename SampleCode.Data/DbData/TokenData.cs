﻿using SampleCode.Data.EF;
using SampleCode.Data.Interfaces;
using SampleCode.Log;
using  System.Linq;

namespace SampleCode.Data.DbData
{
    public class TokenData : ITokenData
    {
        private readonly ILogger _logger;


        public TokenData(ILogger logger)
        {
            _logger = logger;
        }

        public bool CheckIfUserExist(string userName, string password)
        {
            using (var dbEntities = new SuitSupplyEntities())
            {
                var exists =  dbEntities.ServiceUsers.FirstOrDefault(x=> x.ServiceUserName == userName && x.ServicePassword == password && x.IsActive.Value);

                return exists != null;
            }
        }
    }
}
