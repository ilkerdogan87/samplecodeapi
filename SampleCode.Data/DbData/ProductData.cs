﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using SampleCode.Data.EF;
using SampleCode.Data.Helpers;
using SampleCode.Data.Interfaces;
using SampleCode.Global.Models.RequestModels.Product;
using SampleCode.Log;

namespace SampleCode.Data.DbData
{
    public class ProductData : IProductData
    {
        private readonly ILogger _logger;

        public ProductData(ILogger logger)
        {
            _logger = logger;
        }

        public List<ProductDataModel> GetProducts(bool onlyActive, string key)
        {
            using (var dbEntities = new SuitSupplyEntities())
            {
                var data = dbEntities.Products.ToList();

                if (onlyActive)
                    data = data.Where(x => x.IsActive).ToList();

                if (!string.IsNullOrWhiteSpace(key))
                    data = data.Where(x => x.Name.Contains(key)).ToList();

                return Mapper.MapTo(data);
            }
        }

        public ProductDataModel GetProductById(int id)
        {
            using (var dbEntities = new SuitSupplyEntities())
            {
                var data = dbEntities.Products.FirstOrDefault(x => x.Id == id);

                return Mapper.MapTo(data);
            }
        }

        public bool AddOrUpdateProduct(ProductDataModel product)
        {
            using (var dbEntities = new SuitSupplyEntities())
            {
                var data = Mapper.MapTo(product);

                dbEntities.Products.AddOrUpdate(data);
                dbEntities.SaveChanges();
                return true;
            }
        }

        public bool RemoveProduct(int id)
        {
            using (var dbEntities = new SuitSupplyEntities())
            {
                var data = dbEntities.Products.FirstOrDefault(x => x.Id == id);

                if (data == null) return true;
                dbEntities.Products.Remove(data);
                dbEntities.SaveChanges();

                return true;
            }
        }
    }
}
