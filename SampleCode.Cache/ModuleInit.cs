﻿using CacheManager.Core;
using SampleCode.Cache.Caches;
using SampleCode.Cache.Interfaces;
using SampleCode.Global.Module;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;

namespace SampleCode.Cache
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(IModuleRegistrar registrar)
        {
            registrar.AddSingleton<ICacheManager<object>>(
                Consts.CacheName.Runtime,
                new InjectionFactory(c =>
                {
                    return CacheFactory.Build<object>(s =>
                    {
                        // Contructed without a name otherwise it not possible to access MemoryCache.Default
                        s.WithSystemRuntimeCacheHandle();
                    });
                }));
            
            registrar.AddSingleton<ICacheRuntime, CacheRuntime>();
            registrar.AddSingleton<ICacheMain, CacheMain>();
            registrar.RegisterType<ICachePerRequest, CachePerRequest>();
        }
    }
}
