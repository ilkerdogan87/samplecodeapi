﻿namespace SampleCode.Cache
{
    public static class Consts
    {
        public class CacheName
        {
            public const string Runtime = "cache.runtime";
        }

        public const string OutputCacheKeyPrefix = "OCACHE:";
    }
}
