﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity.InterceptionExtension;
using SampleCode.Cache.Interfaces;
using ReflectionHelper = SampleCode.Global.Helpers.ReflectionHelper;
using SampleCode.Global.Helpers;

namespace SampleCode.Cache.Interception
{
    public class CachingInterceptorBehavior : IInterceptionBehavior
    {
        private readonly ICacheMain _cache;
        private readonly ICachePerRequest _cachePerRequest;

        public CachingInterceptorBehavior(ICacheMain cache, ICachePerRequest cachePerRequest)
        {
            _cache = cache;
            _cachePerRequest = cachePerRequest;
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Enumerable.Empty<Type>();
        }

        public bool WillExecute => true;

        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {

            IMethodReturn result;
            var cacherAttrCheck = input.MethodBase.GetCustomAttributes(typeof(CachingAttribute), true);
            if (cacherAttrCheck.Any() == false)
                return getNext()(input, getNext);

            var cacheAttr = cacherAttrCheck.FirstOrDefault() as CachingAttribute;
            if (cacheAttr == null)
                return getNext()(input, getNext);

            var cacher = cacheAttr.Cacher;
            
            var cacheKey = ReflectionHelper.GenerateKey(input);

            object cacheResult;

            //Test ortamlarında cache kullanımı kapattık
            if (!ConfigHelper.UseCache)
            {
                result = getNext()(input, getNext);
                return result;
            }

            if (CacheManager.IsCacheDisabledPerRequest(_cachePerRequest) == false && _cache.Get(cacheAttr.Cacher, cacheKey, out cacheResult))
            {
                result = input.CreateMethodReturn(cacheResult, input.Arguments);
            }
            else
            {
                result = getNext()(input, getNext);

                if (result.Exception == null && result.ReturnValue != null)
                {
                    _cache.Set(cacheAttr.Cacher, cacheKey, result.ReturnValue, TimeSpan.FromSeconds(cacheAttr.Duration));
                }
            }

            return result;
        }
    }
}
