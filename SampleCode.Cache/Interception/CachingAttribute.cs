﻿using System;

namespace SampleCode.Cache.Interception
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class CachingAttribute : Attribute
    {
        public long Duration { get; set; }
        public Enums.CacheType Cacher { get; set; }
        
        public CachingAttribute()
        {
            Duration = 300;
            Cacher = Enums.CacheType.Runtime;
        }
    }
}
