﻿
using CacheManager.Core;
using SampleCode.Cache.Interfaces;
using Microsoft.Practices.Unity;
using System;

namespace SampleCode.Cache.Caches
{
    internal class CacheRuntime : ICacheRuntime
    {
        private readonly ICacheManager<object> _cache;

        public CacheRuntime([Dependency(Consts.CacheName.Runtime)] ICacheManager<object> cache)
        {
            _cache = cache;
        }

        public void Set<T>(string key, T o, bool disableCacheDependencyTracking = false)
        {
            _cache.Put(new CacheItem<object>(key, o));
        }

        public void Set<T>(string key, T o, TimeSpan duration, bool disableCacheDependencyTracking = false)
        {
            _cache.Put(new CacheItem<object>(key, o, ExpirationMode.Absolute, duration));
        }

        public T Get<T>(string key, bool disableCacheDependencyTracking = false)
        {
            return _cache.Get<T>(key);
        }

        public bool Get<T>(string key, out T o, bool disableCacheDependencyTracking = false)
        {
            bool result;
            o = default(T);

            try
            {
                if (Exists(key) == false)
                {
                    result = false;
                }
                else
                {
                    o = Get<T>(key);
                    result = true;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public bool Exists(string key)
        {
            return _cache.Exists(key);
        }

        public void Delete(string key)
        {
            _cache.Remove(key);
        }

        public void DeleteAll()
        {
            _cache.Clear();
        }

        public void Dispose()
        {
            
        }
    }
}
