﻿using SampleCode.Cache.Interfaces;
using System;

namespace SampleCode.Cache.Caches
{
    internal class CacheMain : ICacheMain
    {
        private readonly ICacheRuntime _cacheRuntime;

        public CacheMain(ICacheRuntime cacheRuntime)
        {
            _cacheRuntime = cacheRuntime;
        }

        public void Set<T>(Enums.CacheType cacheType, string key, T o, bool disableCacheDependencyTracking = false)
        {
            switch (cacheType)
            {
                case Enums.CacheType.Runtime:
                    _cacheRuntime.Set(key, o, disableCacheDependencyTracking);
                    break;
            }
        }

        public void Set<T>(Enums.CacheType cacheType, string key, T o, TimeSpan duration, bool disableCacheDependencyTracking = false)
        {
            switch (cacheType)
            {
                case Enums.CacheType.Runtime:
                    _cacheRuntime.Set(key, o, duration, disableCacheDependencyTracking);
                    break;

            }
        }

        public T Get<T>(Enums.CacheType cacheType, string key, bool disableCacheDependencyTracking = false)
        {
            var result = default(T);

            switch (cacheType)
            {
                case Enums.CacheType.Runtime:
                    result = _cacheRuntime.Get<T>(key, disableCacheDependencyTracking);
                    break;
            }

            return result;
        }

        public bool Get<T>(Enums.CacheType cacheType, string key, out T o, bool disableCacheDependencyTracking = false)
        {
            var result = false;
            o = default(T);

            switch (cacheType)
            {
                case Enums.CacheType.Runtime:
                    result = _cacheRuntime.Get(key, out o, disableCacheDependencyTracking);
                    break;
            }

            return result;
        }

        public bool Exists(Enums.CacheType cacheType, string key)
        {
            var result = false;

            switch (cacheType)
            {
                case Enums.CacheType.Runtime:
                    result = _cacheRuntime.Exists(key);
                    break;
            }

            return result;
        }

        public void Delete(Enums.CacheType cacheType, string key)
        {
            switch (cacheType)
            {
                case Enums.CacheType.Runtime:
                    _cacheRuntime.Delete(key);
                    break;
            }
        }

        public void DeleteAll(Enums.CacheType cacheType)
        {
            switch (cacheType)
            {
                case Enums.CacheType.Runtime:
                    _cacheRuntime.DeleteAll();
                    break;
            }
        }
    }
}
