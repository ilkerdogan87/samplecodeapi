﻿

using SampleCode.Cache.Interfaces;
using System.Web;

namespace SampleCode.Cache.Caches
{
    internal class CachePerRequest : ICachePerRequest
    {
        private const string Prefix = "rc_";
        private readonly HttpContextBase _httpContext;

        public CachePerRequest(HttpContextBase httpContext)
        {
            _httpContext = httpContext;
        }
        
        public void Set<T>(string key, T o)
        {
            _httpContext.Items[Prefix + key] = o;
        }

        public T Get<T>(string key)
        {
            var result = default(T);
            var item = _httpContext.Items[Prefix + key];
            if (item == null)
                return result;

            return (T)item;
        }

        public void Remove(string key)
        {
            if (_httpContext.Items.Contains(Prefix + key) == false)
                return;

            _httpContext.Items.Remove(Prefix + key);
        }
    }
}
