﻿namespace SampleCode.Cache.Interfaces
{
    public interface ICachePerRequest
    {
        void Set<T>(string key, T o);
        T Get<T>(string key);
        void Remove(string key);
    }
}
