﻿using System;

namespace SampleCode.Cache.Interfaces
{
    /// <summary>
    /// Handles Runtime Cache
    /// </summary>
    public interface ICacheMain
    {
        /// <summary>
        /// Infinite Cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheType"></param>
        /// <param name="key"></param>
        /// <param name="o"></param>
        /// <param name="disableCacheDependencyTracking"></param>
        void Set<T>(Enums.CacheType cacheType, string key, T o, bool disableCacheDependencyTracking = false);

        /// <summary>
        /// Reference type olmayan dataları cache'e atmak için kullanılır
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheType"></param>
        /// <param name="key"></param>
        /// <param name="o"></param>
        /// <param name="duration"></param>
        /// <param name="disableCacheDependencyTracking"></param>
        void Set<T>(Enums.CacheType cacheType, string key, T o, TimeSpan duration, bool disableCacheDependencyTracking = false);

        /// <summary>
        /// Get By Key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheType"></param>
        /// <param name="key"></param>
        /// <param name="disableCacheDependencyTracking"></param>
        /// <returns></returns>
        T Get<T>(Enums.CacheType cacheType, string key, bool disableCacheDependencyTracking = false);

        /// <summary>
        /// Try Get by Key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheType"></param>
        /// <param name="key"></param>
        /// <param name="o"></param>
        /// <param name="disableCacheDependencyTracking"></param>
        /// <returns></returns>
        bool Get<T>(Enums.CacheType cacheType, string key, out T o, bool disableCacheDependencyTracking = false);

        /// <summary>
        /// Exists
        /// </summary>
        /// <param name="cacheType"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Exists(Enums.CacheType cacheType, string key);

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="cacheType"></param>
        /// <param name="key"></param>
        void Delete(Enums.CacheType cacheType, string key);
        /// <summary>
        /// Delete All
        /// </summary>
        void DeleteAll(Enums.CacheType cacheType= Enums.CacheType.Runtime);
    }
}
