﻿using System;
using System.Collections.Generic;
using CacheManager.Core;
using SampleCode.Global.Module;
using SampleCode.Cache.Interfaces;
using System.Linq.Expressions;
using ReflectionHelper = SampleCode.Global.Helpers.ReflectionHelper;
using System.Threading.Tasks;

namespace SampleCode.Cache
{
    public class CacheManager
    {
        private const string KeyRequestCacheIgnoreCache = "cache_ignore";
        private const string KeyRequestCacheItemsToBeUpdated = "cache_update_items";
        private const string KeyRedisChannelNameForUpdate = "cache_update";

        internal const string DependentKeyPrefix = "DEP:";
        private const string DependentKeyRequestCache = "cache_dep";

        #region Cache Dependency Helpers

        internal static void CacheDependencySet(string key, bool disableCacheDependencyTracking)
        {
            if (disableCacheDependencyTracking)
                return;

            var cacheRequest = ModuleResolver.Resolve<ICachePerRequest>();

            var keys = cacheRequest.Get<List<string>>(DependentKeyRequestCache);

            keys.Add(key);
            cacheRequest.Set(DependentKeyRequestCache, keys);
        }

        /// <summary>
        /// This runs only if cache per request is disabled!
        /// Cache is only disabled when we want to update cache items without removing them from cache.
        /// While this is happening we need to track cache dependency no matter what.
        /// Only run this when cache is disabled per request!
        /// </summary>
        /// <param name="key"></param>
        internal static void CacheDependencyForCacheUpdate(string key)
        {
            var cacheRequest = ModuleResolver.Resolve<ICachePerRequest>();

            // Do not let this method run if cache is not disabled for the request
            var isCacheDisabledForRequest = IsCacheDisabledPerRequest(cacheRequest);

            if (isCacheDisabledForRequest == false)
                return;

            var keys = cacheRequest.Get<List<string>>(DependentKeyRequestCache);

            cacheRequest.Set(DependentKeyRequestCache, keys);
        }

        #endregion

        private static void EnableCachePerRequest(ICachePerRequest cacheRequest)
        {
            if (cacheRequest == null)
                throw new ArgumentNullException(nameof(cacheRequest));

            cacheRequest.Set(KeyRequestCacheIgnoreCache, false);
        }

        private static void DisableCachePerRequest(ICachePerRequest cacheRequest)
        {
            if (cacheRequest == null)
                throw new ArgumentNullException(nameof(cacheRequest));

            cacheRequest.Set(KeyRequestCacheIgnoreCache, true);
        }

        public static bool IsCacheDisabledPerRequest(ICachePerRequest cacheRequest)
        {
            if (cacheRequest == null)
                throw new ArgumentNullException(nameof(cacheRequest));

            return cacheRequest.Get<bool>(KeyRequestCacheIgnoreCache);
        }

        private static Dictionary<string, CacheItem<object>> GetCacheItemsToBeUpdated(ICachePerRequest cacheRequest)
        {
            return cacheRequest.Get<Dictionary<string, CacheItem<object>>>(KeyRequestCacheItemsToBeUpdated);
        }
    }

    public class CacheUpdateModel
    {
        public string Key { get; set; }
        public CacheItem<object> Item { get; set; }
        public string Publisher { get; set; }
    }
}
