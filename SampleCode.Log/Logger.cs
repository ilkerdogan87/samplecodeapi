﻿using SampleCode.Global;
using SampleCode.Global.Helpers;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using SampleCode.Global.Models.RequestModels.Token;

namespace SampleCode.Log
{
    internal class Logger : ILogger
    {
        private static void Log(LogModel model, int lineNumber, string caller, string callerPath)
        {
            if (model == null)
                return;
            
            try
            {
                var tokenModel = (TokenRequestModel)HttpContext.Current.Items["tokenModel"];

                if (tokenModel != null)
                {
                    model.Platform = GeneralFunctions.ConvertToPlatformType(tokenModel.Platform);
                    model.IpAddress = tokenModel.ClientIp;
                }

                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        new LoggerService().Log(model);
                    }
                    catch(Exception ex)
                    {
                        //farklı bir yere log atmak gerekebilir
                    }
                });
            }
            catch
            {
                //farklı bir yere log atmak gerekebilir
            }
        }

        public void Info(LogModel model, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string callerPath = null)
        {
            model.EventType = Enums.LogType.Info;
            Log(model, lineNumber, caller, callerPath);
        }

        public void Warn(LogModel model, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string callerPath = null)
        {
            model.EventType = Enums.LogType.Warn;
            Log(model, lineNumber, caller, callerPath);
        }

        public void Error(LogModel model, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string callerPath = null)
        {
            model.EventType = Enums.LogType.Error;
            Log(model, lineNumber, caller, callerPath);
        }
    }
}
