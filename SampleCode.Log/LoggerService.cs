﻿using System;
using System.Text;

namespace SampleCode.Log
{
    public class LoggerService
    {
        //private readonly LogManagerClient _tpmLogServiceClient;
        
        public LoggerService()
        {

            try
            {
                //_logManagerClient = new LogManagerClient();
                //_logManagerClient.Endpoint.Address = ServiceUrlHelper.GetEndpointAddress();

            }
            catch (Exception ex)
            {
                //Loglama yaparken hata alırken tekrar log atmaya çalışma :)
            }
        }

        public void Log(LogModel log)
        {
            if (log == null)
                return;

            //var model = FillLogModel(log);
            var model = new LogModel();
            if (model == null)
                return;

            //using (new OperationContextScope(_logManagerClient.InnerChannel))
            //{
            //    _logManagerClient.Log(model);
            //}
        }

        private string FillExtra(LogModel log)
        {
            if (log == null)
                return null;

            var sb = new StringBuilder();

            sb.Append("input:{").Append(log.Input).Append("},")
                .Append("output:{").Append(log.Result).Append("},")
                .Append("other:{").Append(log.Extra).Append("},");

            return sb.ToString();

        }

        //private PRMConstantsPTPMLogModel FillLogModel(LogModel log)
        //{
        //    if (log == null)
        //        return null;
            
        //    return new PRMConstantsPTPMLogModel
        //    {
        //        Msisdn = log.Msisdn,
        //        Platform = ConvertToPlatform(log.Platform),
        //        IpAddress = log.IpAddress,
        //        LogType = ConvertToLogType(log.EventType),
        //        Exception = log.Exception != null ?  log.Exception.ToString() : null,
        //        MethodName = log.MethodName,
        //        Message = log.Message,
        //        ApplicationVersion = log.ApplicationVersion,
        //        Device = log.Device,
        //        DeviceVersion = log.DeviceVersion,
        //        UserPhoneNumber = log.Msisdn,
        //        Extra = FillExtra(log)
        //    };
        //}
        
        //private PRMConstantsPlatformType ConvertToPlatform(Enums.PlatformType platformType)
        //{
        //    switch (platformType)
        //    {
        //        case Enums.PlatformType.IOS:
        //            return PRMConstantsPlatformType.Ios;
        //        case Enums.PlatformType.ANDROID:
        //            return PRMConstantsPlatformType.WindowsPhone;
        //        default:
        //            return PRMConstantsPlatformType.Ios;
        //    }
        //}

        //private PRMConstantsLogType ConvertToLogType(Enums.LogType logType)
        //{
        //    switch (logType)
        //    {
        //        case Enums.LogType.Warn:
        //            return PRMConstantsLogType.Warn;
        //        case Enums.LogType.Error:
        //            return PRMConstantsLogType.Error;
        //        case Enums.LogType.Info:
        //            return PRMConstantsLogType.Info;
        //        default:
        //            return PRMConstantsLogType.None;
        //    }
        //}
    }
}
