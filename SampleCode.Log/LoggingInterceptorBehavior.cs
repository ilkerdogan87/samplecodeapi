﻿using SampleCode.Global;
using SampleCode.Global.Helpers;
using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using SampleCode.Global.Models;

namespace SampleCode.Log
{
    public class LoggingInterceptorBehavior : IInterceptionBehavior
    {
        private readonly ILogger _logger;

        public LoggingInterceptorBehavior(ILogger logger)
        {
            _logger = logger;
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Enumerable.Empty<Type>();
        }

        public bool WillExecute => true;

        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            var inputs = Global.Helpers.ReflectionHelper.GetInputParametersWithValues(input);
            string fullMethodName;

            if (input.MethodBase.DeclaringType != null)
                fullMethodName = input.MethodBase.DeclaringType.FullName + "$" + input.MethodBase.Name;
            else
                fullMethodName = input.MethodBase.Name;


            var result = getNext()(input, getNext);

            var exception = result.Exception;
            
            var logRequests = ConfigHelper.LogServiceRequests;
            
            if (logRequests)
            {
                _logger.Info(new LogModel { MethodName = fullMethodName, Message = "ServiceCall", Input = inputs,Result = GeneralFunctions.ToJSON(result.ReturnValue) });
            }
            

            if (exception != null)
            {
                _logger.Error(new LogModel { MethodName = fullMethodName,Message =exception.Message, Exception = exception,Input = inputs });

                if (exception is ApiException)
                {
                    result.Exception = exception;
                }
                else
                {
                    throw new Exception(Consts.Messages[Enums.MessageType.DefaultErrorMessage]);
                }
            }

            return result;
        }

    }
}
