﻿using System.Runtime.CompilerServices;

namespace SampleCode.Log
{
    public interface ILogger
    {
        void Info(LogModel model, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string callerPath = null);
        void Warn(LogModel model, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string callerPath = null);
        void Error(LogModel model, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string callerPath = null);
    }
}
