﻿using SampleCode.Global;

namespace SampleCode.Log
{
    public class LogModel
    {
        public Enums.LogType EventType { get; internal set; }
        public string MethodName { get; set; }
        public string Message { get; set; }
        public Enums.PlatformType Platform { get; internal set; }
        public string Msisdn { get; internal set; }
        public string Device { get; set; }
        public string DeviceVersion { get; set; }
        public string ApplicationVersion { get; set; }
        public string IpAddress { get; internal set; }
        public object Exception { get; set; }
        public object Extra { get; set; }
        public string Input { get; set; }
        public object Result { get; set; }
        //public string CodeCaller { get; internal set; }
        //public string CodeCallerPath { get; internal set; }
        //public int CodeLineNumber { get; internal set; }

    }
}
