﻿using EricssonTpm.Global.Helpers;
using System.ServiceModel;

namespace EricssonTpm.Log
{
    internal static class LogServiceUrlHelper
    {
        private static string GetServiceUrl()
        {
            var url = ConfigHelper.LogService;

            var deploymentMode = ConfigHelper.DeploymentMode;
            var response = string.Format(url, deploymentMode);

            return response;
        }

        public static EndpointAddress GetEndpointAddress()
        {
            return new EndpointAddress(GetServiceUrl());
        }
    }
}
