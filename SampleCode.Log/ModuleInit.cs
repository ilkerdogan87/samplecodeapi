﻿using SampleCode.Global.Module;
using System.ComponentModel.Composition;

namespace SampleCode.Log
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(IModuleRegistrar registrar)
        {
            registrar.AddSingleton<ILogger, Logger>();
        }
    }
}
